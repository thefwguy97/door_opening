// Rotary Dial by TheFwGuy - May 2020
// The program is reading an old PSTN Rotary Dial and activate a relay for 1 second if the user compose a specific number
// (default is 23) - max 10 digits.
//
// Is possible to use a Neopixel strip as feedback about the reading of the PSTN rotary dial
//
// A LED is used for these purposes :
//  ON it indicate that the system is ready to compose a number
//  OFF it indicate that the system is acquiring the number
//  FLASH it indicate the system is acquiring the number

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// ----- Defines 
#define LED_PIN    1    // LED or Neopixel data (if defined USE_NEOPIXEL)
#define SW1        2    // Signalling switch
#define SW2        0    // Pulse dial switch
#define RELAY      4    // Relay

// Reading number state machine
#define RN_START  0
#define RN_IDLE   1
#define RN_OPEN   2
#define RN_END    3

// Reading dial state machine
#define RD_START  0 
#define RD_IDLE   1
#define RD_COUNT  2
#define RD_CHECK  3
#define RD_END    4

//#define USE_NEOPIXEL
#define LED_COUNT 16    // For neopixel
#define MAX_LEN_NUM  10  // Max number of digits

#define END_COMPOSITION_TIME  3000       // Time to end the number composition - ~3 sec
#define DETECT_PROGRAMMING_TIME  6000    // Time to end the number composition - ~6 seconds
// ------ Global variables

#if defined USE_NEOPIXEL
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
#endif

char rn_status;       // Number reading state machine 
char rd_status;       // Rotary dial reading state machine 
char active_number;   // Number being read       
char number[MAX_LEN_NUM];  // Array to store read numbers (max MAX_LEN_NUM digits)
char stored_number[MAX_LEN_NUM];

char num_index;       // Index where to store the number
char isProgramming;   // Flag to indicate to program the number

unsigned long currentMillis = 0;    // stores the value of millis() in each iteration of loop()
unsigned long digitTimerMillis = 0;   // will count for waiting the end of the digit (indicate the end of composition)
unsigned long programmingStartTimerMillis = 0;   // used to determine if the composed number is the programming one

// setup() function -- runs once at startup --------------------------------

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  rn_status = RN_START;
  rd_status = RD_START;

  num_index = 0;
  isProgramming = 0;

  // Forcing default number to check - 118
  reset_stored_number();
  stored_number[0] = 1;
  stored_number[1] = 1;
  stored_number[2] = 8;

 
  // Set input pins for rotary dial
  pinMode(SW1,INPUT);
  pinMode(SW2,INPUT);
  pinMode(RELAY, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  digitalWrite(RELAY, LOW);
  digitalWrite(LED_PIN, LOW);

#if defined USE_NEOPIXEL
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(15); // Set BRIGHTNESS to about 1/5 (max = 255)

  // Test strip
  //colorWipe(strip.Color(  0, 255,   0), 80); // Green
  //colorWipe(strip.Color(  255, 0,   0), 80); // Green
  //colorWipe(strip.Color(  0,   0,   255), 80); // Green
#endif
 }


// loop() function -- runs repeatedly as long as board is on ---------------

void loop() {
   currentMillis = millis();    // Update time base
  
  switch (rn_status) {
    case RN_START:
    default:
      reset_number();       // Reset area where to save number
      isProgramming = 0;    // Reste flag for programming
      num_index = 0;
      rn_status = RN_IDLE;
      rd_status = RD_START;
      break;

    case RN_IDLE:
      rd_state_machine();
      break;

    case RN_OPEN:
      // Check number !!! 
      if (check_number() == true) {
         open_door();
      }
      rn_status = RN_START;
      break;
  }
}

// State machine to read the dial
void rd_state_machine () {
  switch (rd_status) {
    case RD_START:
    default:
      active_number = 0;
#if defined USE_NEOPIXEL
      colorWipe(strip.Color(  0, 255,   0), 10); // Green
#endif
      rd_status = RD_IDLE;
      break;

    case RD_IDLE:
      if (digitalRead(SW1) == LOW) {
#if defined USE_NEOPIXEL
        colorWipe(strip.Color(255,   0,   0), 5); // Red
#endif
        // Set timer for end acquiring number
        digitTimerMillis = currentMillis;

        // Set timer for detect if is a programming session
        programmingStartTimerMillis = currentMillis;

        rd_status = RD_COUNT;
      }
      break;  

    case RD_COUNT:
      if (digitalRead(SW1) == LOW) {

        // Check here if to set the programming flag - only on the first digit !!
        if (num_index == 0 && isProgramming == 0) {
          if (currentMillis - programmingStartTimerMillis > DETECT_PROGRAMMING_TIME)
          {
             reset_stored_number();
             isProgramming = 1;
             // Set timer again for detecting end of number since the programming timer screw it up
             digitTimerMillis = currentMillis;
             digitalWrite(LED_PIN, HIGH);   // Turn LED on
          }
        }

        if (readSwitch(SW2) ) { 
          active_number++;
          digitalWrite(LED_PIN, HIGH);
          while (readSwitch(SW2));   // Wait the pulse to go down
          digitalWrite(LED_PIN, LOW);

#if defined USE_NEOPIXEL
          colorNumber(strip.Color(  0, 0, 255), 0, active_number);        
#endif
        }
      } else {
        // Number finished
#if defined USE_NEOPIXEL
        colorWipe(strip.Color(  0, 0, 0), 0); // turn off display
        colorNumber(strip.Color(  0, 0, 255), 0, active_number);
#endif
        // The digit was correctly read
        // If in programming mode save the digit in the programming array
        if (isProgramming == 1) {
           if (num_index < MAX_LEN_NUM) {
              stored_number[num_index] = active_number;
           }
        } else {
          // save it in array if there is space
           if (num_index < MAX_LEN_NUM) {
              number[num_index] = active_number;
           }
        }
        num_index++;          // Point to the next digit
        active_number = 0;    // Reset active_number for the next read
        rd_status = RD_CHECK;
      }
      break;

      case RD_CHECK:
        // Here wait for a timer expiration to detect if the composition is done or go back
        if (digitalRead(SW1) == LOW) {
#if defined USE_NEOPIXEL
          colorWipe(strip.Color(255,   0,   0), 5); // Red
#endif
          // Set timer
          digitTimerMillis = currentMillis;

          rd_status = RD_COUNT;
        } else {
          if (currentMillis - digitTimerMillis > END_COMPOSITION_TIME)
          {
            rd_status = RD_END;
            if (isProgramming == 0) {
               rn_status = RN_OPEN;    // Indicate the number is finished
            } else {
              // Programming mode !  Just restart everything 
              programming_ok();
              rn_status = RN_START;
            }
          }
        }
        break;

     case RD_END:
        // Do noting here - wait for the other state machine to restart the cycle
        break;   
   }  
}

// Reset the area where the number is stored
void reset_number() {
  for (int i=0; i<MAX_LEN_NUM-1; i++)
     number[i]=0x00;
}

// Reset the area where the number to compare is stored
void reset_stored_number() {
  for (int i=0; i<MAX_LEN_NUM-1; i++)
     stored_number[i]=0x00;
}

// Check if the composed number is equal to the stored one 
bool check_number() {
  for (int i=0; i<MAX_LEN_NUM-1; i++) {
     if (number[i] != stored_number[i])
        return false;
  }
  return true;
}

// Function to activate the open door relay
void open_door() {
   digitalWrite(LED_PIN, HIGH);

   digitalWrite(RELAY, HIGH);
   delay(1000);
   digitalWrite(RELAY, LOW);        

   digitalWrite(LED_PIN, LOW);
}

// Function to ack operation
void programming_ok() {
   digitalWrite(LED_PIN, HIGH);

   delay(500);

   digitalWrite(LED_PIN, LOW);
}
// Read a button with some debouncing - return true if the pin goes high.
// returns false otherwise
boolean readSwitch(int pin)
{
   if(digitalRead(pin))
   {
      delay(1);
      if(digitalRead(pin))
      {
         return true;
      }
   }
   return false;
}

#if defined USE_NEOPIXEL
// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

// Display a number (from 0 to 8)
void colorNumber(uint32_t color, int wait, char number) {
  for(int i=0; i<number; i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}
#endif
